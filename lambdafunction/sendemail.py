import boto3
from botocore.exceptions import ClientError
import json

def lambda_handler(event, context):
    
    #request variables
    to = event['Request']['Email']['to']
    _from = event['Request']['Email']['from']
    subject = event['Request']['Email']['subject']
    body = event['Request']['Email']['body']
    
    
    SENDER = _from
    RECIPIENT = to
    AWS_REGION = "eu-west-1"
    SUBJECT = subject
    CHARSET = "UTF-8"
    
    BODY_HTML = f"""<html>
    <head></head>
    <body>
    <h1>Show Me the Weather!</h1>
    <p>{body}</p>
      </body>
      </html>
      """            
    
    # Create a new SES resource and specify a region.
    client = boto3.client('ses',region_name=AWS_REGION)

    # Try to send the email.
    try:
        #Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    }
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )
    # Display an error if something goes wrong.	
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        return event